#versión 1: con los datos en memoria, con templates
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/mustache'

require_relative 'item'


lista = [Item.new(1,"pan",1), Item.new(2,"azucar",1)]

get '/' do
    @lista = lista
    mustache :index
end 
