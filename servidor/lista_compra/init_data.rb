require 'sequel'
require 'logger'

db = Sequel.connect('sqlite://lista.db')
db.loggers << Logger.new($stdout)
items = db[:items]
items.delete
items.insert(:nombre=>'pan', :cantidad=>2)
items.insert(:nombre=>'agua', :cantidad=>1)
items.insert(:nombre=>'huevos', :cantidad=>12)
