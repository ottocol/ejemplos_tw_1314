#versión 1: con los datos en memoria y sin usar templates
require 'sinatra'
require 'sinatra/reloader'
require_relative 'item'


lista = [Item.new(1,"pan",1), Item.new(2,"azucar",1)]

get '/' do
    html = "<h1>Lista de la compra</h1>"
    html += "<ul>"
    lista.each do |i|
        html += "<li> #{i.nombre} #{i.cantidad} </li>"
    end
    html += "</ul>"
end 
