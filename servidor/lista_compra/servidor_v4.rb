#encoding: utf-8
#versión 4: REST, con base de datos (sqlite con sequel)
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/mustache'

require 'sequel'
require 'logger'
require 'json'
require 'sequel/plugins/serialization'



configure do
    db = Sequel.connect('sqlite://lista.db')
    db.loggers << Logger.new($stdout)
    class Item < Sequel::Model
      plugin :json_serializer
    end
end

get '/' do
 'Hola mundo'
end

get '/lista' do
    Item.all.to_json
end 

delete '/lista/:id' do
    Item.where(:id=>params[:id]).destroy()
    status 200
    "Item eliminado: #{params[:id]}"
end 

post '/lista' do
  json = request.body.read
  i = Item.from_json(json)
  i.save
  status 201
  "Item guardado: #{i.id}"
end   
