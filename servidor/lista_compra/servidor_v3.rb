#versión 3: con base de datos (sqlite con sequel), con templates
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/mustache'

#nuevos (o cambiados) require
require 'sequel'
require 'logger'


configure do
    db = Sequel.connect('sqlite://lista.db')
    db.loggers << Logger.new($stdout)
    #cuando se carga la clase, sequel busca la BD por narices, así que hay que conectar ANTES 
    #http://stackoverflow.com/questions/13115550/defining-sequel-models-before-connecting
    require_relative 'item_sequel'
end

get '/' do
    @lista = Item.all
    puts @lista
    mustache :index
end 

get '/borrarItem' do
    Item.where(:id=>params[:id]).destroy()
    "Item eliminado <br>" +
    '<a href="/">Volver a la lista</a>'
end 

post '/nuevoItem' do
  i = Item.new
  i.nombre = params[:nombre]
  i.cantidad = params[:cantidad].to_i
  i.save
  'Item guardado <br> <a href="/">Volver a la lista</a>'
end   
