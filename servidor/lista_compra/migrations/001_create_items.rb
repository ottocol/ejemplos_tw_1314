Sequel.migration do
  change do
    create_table(:items) do
      primary_key :id
      String :nombre, :null=>false
      Integer :cantidad
    end
  end
end