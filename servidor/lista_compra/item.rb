class Item
   attr_accessor :id, :nombre, :cantidad

   def initialize(id, nombre, cantidad)
        self.id = id
        self.nombre = nombre
        self.cantidad = cantidad
   end
end