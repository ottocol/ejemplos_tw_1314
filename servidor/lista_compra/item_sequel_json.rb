require 'json'
require 'sequel/plugins/serialization'

class Item < Sequel::Model
    plugin :json_serializer
end